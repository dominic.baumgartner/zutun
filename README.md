# Zutun ✅

A playground for different programming languages. The goal is to implement a simple to-do service
back-end in different languages.

## Features
- Todo API with CRUD operations
- Background worker for sending daily reminders

## Technical Requirements
- Todos are stored in a database
- The background worker can be configured using a cron expression
- Secrets are stored in environment variables
- The API can be containerized

## Languages 

| Language   | Status |
| ---------- | ------ |
| Python     |        |
| C#         |        |
| Rust       |        |
| Go         |        |
| Ruby       |        |
| Kotlin     |        |
| TypeScript |        |