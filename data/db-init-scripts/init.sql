CREATE TABLE todos (
    id SERIAL PRIMARY KEY,
    task VARCHAR(255),
    due_by TIMESTAMP,
    completed BOOLEAN
);

INSERT INTO todos (task, due_by, completed)
VALUES ('Do very important stuff', NOW() + INTERVAL '1 day', FALSE),
       ('Learn flask', NOW() + INTERVAL '2 days', FALSE),
       ('Build a todo API', NOW() + INTERVAL '7 days', FALSE);